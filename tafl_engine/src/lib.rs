/*
 * Copyright (C) 2021-2023  Andrej Trnkóci
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::cmp;

// Warning changing width and height can cause problems, TBD fix
const WIDTH: usize = 7;
const HEIGHT: usize = 7;

const XCOORDS: &str = "ABCDEFGHI";
const YCOORDS: &str = "123456789";

#[derive(Clone, Debug, PartialEq, Copy)]
pub enum PieceType {
    Attacker,
    Defender,
    King,
}

#[derive(Clone, Debug, PartialEq, Copy)]
pub enum Players {
    Attackers,
    Defenders,
}

impl Default for Players {
    fn default() -> Self {Players::Attackers}
}

#[derive(Clone, Debug, Default)]
pub struct Board {
    pub tile_matrix: [[Option<PieceType>; HEIGHT]; WIDTH],
    pub current_player: Players,
    pub winner: Option<Players>,
}

impl Board {
    pub fn new() -> Board {
        Board {
            tile_matrix: [[None; HEIGHT]; WIDTH],
            current_player: Players::Attackers,
            winner: None,
        }
    }

    pub fn set_up(&mut self) {
        for x in 0 .. WIDTH {
            for y in 0 .. HEIGHT {
                self.tile_matrix[x][y] = None;
            }
        }
        let center_x = WIDTH / 2;
        let center_y = HEIGHT / 2;
        self.tile_matrix[center_x][center_y] = Some(PieceType::King);
        self.tile_matrix[center_x + 1][center_y] = Some(PieceType::Defender);
        self.tile_matrix[center_x - 1][center_y] = Some(PieceType::Defender);
        self.tile_matrix[center_x][center_y + 1] = Some(PieceType::Defender);
        self.tile_matrix[center_x][center_y - 1] = Some(PieceType::Defender);
        self.tile_matrix[center_x + 2][center_y] = Some(PieceType::Attacker);
        self.tile_matrix[center_x - 2][center_y] = Some(PieceType::Attacker);
        self.tile_matrix[center_x][center_y + 2] = Some(PieceType::Attacker);
        self.tile_matrix[center_x][center_y - 2] = Some(PieceType::Attacker);
        self.tile_matrix[center_x + 3][center_y] = Some(PieceType::Attacker);
        self.tile_matrix[center_x - 3][center_y] = Some(PieceType::Attacker);
        self.tile_matrix[center_x][center_y + 3] = Some(PieceType::Attacker);
        self.tile_matrix[center_x][center_y - 3] = Some(PieceType::Attacker);
        self.current_player = Players::Attackers;
        self.winner = None;
    }

    pub fn print(&self) {
        for h_idx in (0..HEIGHT).rev() {
            print!("{}", h_idx + 1);
            for w_idx in 0..WIDTH {
                match self.tile_matrix[w_idx][h_idx] {
                    Some(piece) => {
                        if piece == PieceType::Attacker {
                            print!("\u{265F}");
                        } else if piece == PieceType::Defender {
                            print!("\u{2659}");
                        } else if piece == PieceType::King {
                            print!("\u{2654}");
                        }
                    }
                    None => {
                        if Board::is_corner(w_idx, h_idx) || Board::is_throne(w_idx, h_idx) {
                            print!("x");
                        } else {
                            print!("-");
                        }
                    }
                }
            }
            println!();
        }
        println!(" ABCDEFG");
    }

    pub fn is_throne(x: usize, y: usize) -> bool {
        x == WIDTH / 2 && y == HEIGHT / 2
    }

    pub fn is_corner(x: usize, y: usize) -> bool {
        (x == 0 || x == WIDTH - 1) && (y == 0 || y == HEIGHT - 1)
    }

    fn is_coord_valid(x: usize, y: usize) -> bool {
        x < WIDTH && y < HEIGHT
    }

    fn is_on_or_next_to_throne(x: usize, y: usize) -> bool {
        let x_i = x as isize;
        let y_i = y as isize;
        let throne_x = WIDTH as isize / 2;
        let throne_y = HEIGHT as isize / 2;
        let dist_x = throne_x - x_i;
        let dist_y = throne_y - y_i;
        let steps_distance = dist_x.abs() + dist_y.abs();
        steps_distance <= 1
    }

    fn is_tile_hostile(&self, piece: PieceType, x: usize, y: usize) -> bool {
        let tile = self.get_tile_content(x, y);
        match tile {
            Some(other_piece) => {
                return Board::are_pieces_enemies(piece, other_piece);
            }
            None => {
                return Board::is_throne(x, y) || Board::is_corner(x, y);
            }
        }
    }

    fn are_pieces_enemies(piece: PieceType, other_piece: PieceType) -> bool {
        Board::get_piece_player(piece) != Board::get_piece_player(other_piece)
    }

    fn get_piece_player(piece: PieceType) -> Players {
        match piece {
            PieceType::King => Players::Defenders,
            PieceType::Defender => Players::Defenders,
            PieceType::Attacker => Players::Attackers,
        }
    }

    pub fn get_tile_content(&self, x: usize, y: usize) -> Option<PieceType> {
        if Board::is_coord_valid(x, y) {
            return self.tile_matrix[x][y].clone();
        } else {
            return None;
        }
    }

    fn can_move(&self, start_x: usize, start_y: usize, dest_x: usize, dest_y: usize) -> bool {
        if Board::is_coord_valid(start_x, start_y) && Board::is_coord_valid(dest_x, dest_y) {
            let is_column_or_row_move: bool = start_x == dest_x || start_y == dest_y;
            let is_target_tile_different: bool = !(start_x == dest_x && start_y == dest_y);
            let is_piece_of_current_player: bool =
                self.is_tile_occupied_by_player(start_x, start_y, self.current_player.clone());
            if Board::is_throne(dest_x, dest_y) || Board::is_corner(dest_x, dest_y) {
                if self.tile_matrix[start_x][start_y] != Some(PieceType::King) {
                    return false;
                }
            }

            if is_column_or_row_move && is_target_tile_different && is_piece_of_current_player {
                //test that there is no piece in a way
                if start_x == dest_x {
                    let max_y = cmp::max(start_y, dest_y);
                    let min_y = cmp::min(start_y, dest_y);
                    for y in min_y..=max_y {
                        if y == start_y {
                            continue;
                        }
                        if self.tile_matrix[start_x][y] != None {
                            return false;
                        }
                    }
                }
                if start_y == dest_y {
                    let max_x = cmp::max(start_x, dest_x);
                    let min_x = cmp::min(start_x, dest_x);
                    for x in min_x..=max_x {
                        if x == start_x {
                            continue;
                        }
                        if self.tile_matrix[x][start_y] != None {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        return false;
    }

    fn is_tile_occupied_by_player(&self, tile_x: usize, tile_y: usize, player: Players) -> bool {
        match self.tile_matrix[tile_x][tile_y] {
            Some(piece_type) => return Board::get_piece_player(piece_type) == player,
            None => return false,
        }
    }

    pub fn is_tile_occupied_by_current_player(&self, tile_x: usize, tile_y: usize) -> bool {
        return self.is_tile_occupied_by_player(tile_x, tile_y, self.current_player);
    }

    fn make_move(&mut self, start_x: usize, start_y: usize, dest_x: usize, dest_y: usize) -> bool {
        if self.can_move(start_x, start_y, dest_x, dest_y) {
            self.tile_matrix[dest_x][dest_y] = self.tile_matrix[start_x][start_y];
            self.tile_matrix[start_x][start_y] = None;
            self.current_player = if self.current_player == Players::Attackers {
                Players::Defenders
            } else {
                Players::Attackers
            };

            self.check_take_piece(dest_x + 1, dest_y, dest_x, dest_y);
            self.check_take_piece(dest_x, dest_y + 1, dest_x, dest_y);
            if dest_y > 0 {
                self.check_take_piece(dest_x, dest_y - 1, dest_x, dest_y);
            }

            if dest_x > 0 {
                self.check_take_piece(dest_x - 1, dest_y, dest_x, dest_y);
            }

            self.evaluate_victory_conditions();

            return true;
        }
        return false;
    }

    pub fn make_move_str_coord(&mut self, move_coord_str: String) -> bool {
        if move_coord_str.len() > 3 {
            //let x_coords = String::from("ABCDEFGHI");
            //let y_coords = String::from("123456789");

            let start_x = XCOORDS.find(move_coord_str.chars().nth(0).unwrap());
            let start_y = YCOORDS.find(move_coord_str.chars().nth(1).unwrap());
            let dest_x = XCOORDS.find(move_coord_str.chars().nth(2).unwrap());
            let dest_y = YCOORDS.find(move_coord_str.chars().nth(3).unwrap());

            match (start_x, start_y, dest_x, dest_y) {
                (Some(sx), Some(sy), Some(dx), Some(dy)) => return self.make_move(sx, sy, dx, dy),
                _ => return false,
            }
        } else {
            return false;
        }
    }

    fn check_take_piece(&mut self, check_x: usize, check_y: usize, moved_x: usize, moved_y: usize) {
        let checked_piece_option = self.get_tile_content(check_x, check_y);

        match checked_piece_option {
            Some(checked_piece) => {
                if checked_piece == PieceType::King
                    && Board::is_on_or_next_to_throne(check_x, check_y)
                {
                    if check_x > 0 && check_y > 0 && check_x < WIDTH - 1 && check_y < HEIGHT - 1 {
                        if self.is_tile_hostile(checked_piece, check_x - 1, check_y)
                            && self.is_tile_hostile(checked_piece, check_x + 1, check_y)
                            && self.is_tile_hostile(checked_piece, check_x, check_y - 1)
                            && self.is_tile_hostile(checked_piece, check_x, check_y + 1)
                        {
                            self.tile_matrix[check_x][check_y] = None;
                        }
                    }
                } else {
                    //fix for underflow of usize. Maybe nicer solution should be used?
                    if 2 * check_x < moved_x {
                        return;
                    }
                    if 2 * check_y < moved_y {
                        return;
                    }

                    let other_x = 2 * check_x - moved_x;
                    let other_y = 2 * check_y - moved_y;

                    if self.is_tile_hostile(checked_piece, moved_x, moved_y)
                        && self.is_tile_hostile(checked_piece, other_x, other_y)
                    {
                        self.tile_matrix[check_x][check_y] = None;
                    }
                }
            }
            None => {
                return;
            }
        }
    }

    fn evaluate_victory_conditions(&mut self) {
        let mut found_king = false;
        let mut king_at_corner = false;
        for x in 0..self.tile_matrix.len() {
            for y in 0..self.tile_matrix[x].len() {
                let current_piece = self.get_tile_content(x, y);
                if current_piece == Some(PieceType::King) {
                    found_king = true;
                    if Board::is_corner(x, y) {
                        king_at_corner = true;
                    }
                }
            }
        }

        if !found_king {
            self.winner = Some(Players::Attackers);
        } else if king_at_corner {
            self.winner = Some(Players::Defenders);
        }

        if self.get_available_moves().len() == 0 {
            self.winner = if self.current_player == Players::Attackers {
                Some(Players::Defenders)
            } else {
                Some(Players::Attackers)
            }
        }
    }

    fn get_available_moves(&self) -> Vec<String> {
        let mut moves: Vec<String> = Vec::new();
        for x in 0..self.tile_matrix.len() {
            for y in 0..self.tile_matrix[x].len() {
                if let Some(current_piece) = self.get_tile_content(x, y) {
                    if Board::get_piece_player(current_piece) == self.current_player {
                        for target_x in 0..self.tile_matrix.len() {
                            if x == target_x {
                                continue;
                            }
                            if self.can_move(x, y, target_x, y) {
                                moves
                                    .push(Board::get_string_move_representation(x, y, target_x, y));
                            }
                        }
                        for target_y in 0..self.tile_matrix[x].len() {
                            if y == target_y {
                                continue;
                            }
                            if self.can_move(x, y, x, target_y) {
                                moves
                                    .push(Board::get_string_move_representation(x, y, x, target_y));
                            }
                        }
                    }
                }
            }
        }
        moves
    }

    fn get_string_move_representation(x1: usize, y1: usize, x2: usize, y2: usize) -> String {
        return format!(
            "{}{}{}{}",
            XCOORDS.as_bytes()[x1] as char,
            YCOORDS.as_bytes()[y1] as char,
            XCOORDS.as_bytes()[x2] as char,
            YCOORDS.as_bytes()[y2] as char
        );
    }
    
    fn heuristics_score(&mut self) -> i32 {
        match self.winner {
            Some(Players::Attackers) => {
                return 10000; 
            }
            Some(Players::Defenders) => {
                return -10000;
            }
            None => {
                let mut score: i32 = 0;
                for x in 0..self.tile_matrix.len() {
                    for y in 0..self.tile_matrix[x].len() {
                        if let Some(PieceType::Attacker) = self.tile_matrix[x][y] {
                            score += 50;                           
                        }
                        if let Some(PieceType::Defender) = self.tile_matrix[x][y] {
                            score -= 100;
                        }
                    }
                }
                return score;
            }
        }
    }
    
    pub fn make_ai_move(&mut self, depth: usize) -> Option<String> {
    	match self.current_player {
    		Players::Attackers => {
    			let (_score, chosen_move) = self.clone().alpha_beta_max(-10001, 10001, depth);
    			if self.make_move_str_coord(chosen_move.clone()) {
    				return Some(chosen_move);
    			}
    			else {
    				return None;
    			}
    			
    		}
    		Players::Defenders => {
    			let (_score, chosen_move) = self.clone().alpha_beta_min(-10001, 10001, depth);
    			if self.make_move_str_coord(chosen_move.clone()) {
    				return Some(chosen_move);
    			}
    			else {
    				return None;
    			}
    		}
    	}
    }
    
    pub fn alpha_beta_max(&mut self, mut alpha: i32, beta: i32, depth: usize) -> (i32, String) {
   	    let mut best_move = String::from("");
        if depth > 0 {
            let moves: Vec<String> = self.get_available_moves();
            for current_move in moves {
                let mut current_board: Board = self.clone();
                current_board.make_move_str_coord(current_move.clone());
                if current_board.winner == Some(Players::Attackers) {
                    return (10000, current_move);
                } else if current_board.winner == Some(Players::Defenders) {
                    return (-10000, current_move);
                }
                let (current_ab_score, _current_ab_move) = current_board.alpha_beta_min(alpha, beta, depth - 1);
                if current_ab_score >= beta {
                    return (current_ab_score, current_move);
                }
                if current_ab_score > alpha {
                    alpha = current_ab_score;
                    best_move = current_move;
                }
            }
            return (alpha, best_move);        
        } else {
            return (self.heuristics_score(), String::from(""));
        }
    }

    pub fn alpha_beta_min(&mut self, alpha: i32, mut beta: i32, depth: usize) -> (i32, String) {
        let mut best_move = String::from("");
        if depth > 0 {
            let moves: Vec<String> = self.get_available_moves();
            for current_move in moves {
                let mut current_board: Board = self.clone();
                current_board.make_move_str_coord(current_move.clone());
                if current_board.winner == Some(Players::Attackers) {
                    return (10000, current_move);
                } else if current_board.winner == Some(Players::Defenders) {
                    return (-10000, current_move);
                }
                let (current_ab_score, _current_ab_move) = current_board.alpha_beta_max(alpha, beta, depth - 1);
                if current_ab_score <= alpha {
                    return (current_ab_score, current_move);
                }
                if current_ab_score < beta {
                    beta = current_ab_score;
                    best_move = current_move;
                }
            }
            return (beta, best_move);    
        } else {
            return (self.heuristics_score(), String::from(""));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn board_tiles_test() {
        assert_eq!(Board::is_throne(3, 3), true);
        assert_eq!(Board::is_throne(0, 3), false);
        assert_eq!(Board::is_corner(0, 0), true);
        assert_eq!(Board::is_corner(0, 6), true);
        assert_eq!(Board::is_corner(2, 2), false);
    }

    #[test]
    fn defender_move_test() {
        let mut board = Board::new();
        board.current_player = Players::Defenders;
        board.tile_matrix[0][0] = Some(PieceType::Defender);
        assert_eq!(board.tile_matrix[0][0], Some(PieceType::Defender));
        board.make_move(0, 0, 0, 3);
        assert_eq!(board.tile_matrix[0][0], None);
        assert_eq!(board.tile_matrix[0][3], Some(PieceType::Defender));
    }

    #[test]
    fn is_tile_occupied_by_player_test() {
        let mut board = Board::new();
        board.tile_matrix[1][0] = Some(PieceType::Defender);
        board.tile_matrix[1][1] = Some(PieceType::Attacker);
        board.tile_matrix[2][0] = Some(PieceType::King);
        assert_eq!(
            board.is_tile_occupied_by_player(1, 0, Players::Defenders),
            true
        );
        assert_eq!(
            board.is_tile_occupied_by_player(1, 0, Players::Attackers),
            false
        );
        assert_eq!(
            board.is_tile_occupied_by_player(1, 1, Players::Defenders),
            false
        );
        assert_eq!(
            board.is_tile_occupied_by_player(1, 1, Players::Attackers),
            true
        );
        assert_eq!(
            board.is_tile_occupied_by_player(2, 0, Players::Defenders),
            true
        );
        assert_eq!(
            board.is_tile_occupied_by_player(2, 0, Players::Attackers),
            false
        );
        assert_eq!(
            board.is_tile_occupied_by_player(2, 1, Players::Defenders),
            false
        );
        assert_eq!(
            board.is_tile_occupied_by_player(2, 1, Players::Attackers),
            false
        );
    }

    #[test]
    fn defenders_take_attacker_test() {
        let mut board = Board::new();
        board.current_player = Players::Defenders;
        board.tile_matrix[1][0] = Some(PieceType::Defender);
        board.tile_matrix[2][0] = Some(PieceType::Attacker);
        board.tile_matrix[3][1] = Some(PieceType::Defender);
        assert_eq!(board.make_move(3, 1, 3, 0), true);
        assert_eq!(board.tile_matrix[2][0], None);
    }

    #[test]
    fn non_king_cannot_enter_throne_test() {
        let mut board = Board::new();
        board.tile_matrix[3][4] = Some(PieceType::Defender);
        board.tile_matrix[3][2] = Some(PieceType::Attacker);
        board.current_player = Players::Defenders;
        assert_eq!(board.make_move(3, 4, 3, 3), false);
        assert_eq!(board.make_move(3, 2, 3, 3), false);
    }

    #[test]
    fn taking_king_on_throne_test() {
        let mut board = Board::new();
        board.tile_matrix[3][3] = Some(PieceType::King);
        board.tile_matrix[2][3] = Some(PieceType::Attacker);
        board.tile_matrix[4][3] = Some(PieceType::Attacker);
        board.tile_matrix[3][2] = Some(PieceType::Attacker);
        board.tile_matrix[4][4] = Some(PieceType::Attacker);
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(4, 4, 3, 4), true);
        assert_eq!(board.tile_matrix[3][3], None);
    }

    #[test]
    fn taking_king_near_throne_test() {
        let mut board = Board::new();
        board.tile_matrix[3][4] = Some(PieceType::King);
        board.tile_matrix[2][3] = Some(PieceType::Attacker);
        board.tile_matrix[4][3] = Some(PieceType::Attacker);
        board.tile_matrix[4][5] = Some(PieceType::Attacker);
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(2, 3, 2, 4), true);
        assert_eq!(board.tile_matrix[3][4], Some(PieceType::King));
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(4, 3, 4, 4), true);
        assert_eq!(board.tile_matrix[3][4], Some(PieceType::King));
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(4, 5, 3, 5), true);
        assert_eq!(board.tile_matrix[3][4], None);
    }

    #[test]
    fn taking_king_far_from_throne_test() {
        let mut board = Board::new();
        board.tile_matrix[0][1] = Some(PieceType::Attacker);
        board.tile_matrix[0][2] = Some(PieceType::King);
        board.tile_matrix[1][3] = Some(PieceType::Attacker);
        assert_eq!(board.make_move(1, 3, 0, 3), true);
        assert_eq!(board.tile_matrix[0][2], None);
    }
    
    #[test]
    fn taking_king_with_corner_test() {
        let mut board = Board::new();
        board.tile_matrix[0][1] = Some(PieceType::King);
        board.tile_matrix[1][2] = Some(PieceType::Attacker);
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(1,2,0,2), true);
        assert_eq!(board.tile_matrix[0][1], None);
    }
    

    #[test]
    fn take_defender_with_throne_test() {
        let mut board = Board::new();
        board.tile_matrix[3][4] = Some(PieceType::Defender);
        board.tile_matrix[4][5] = Some(PieceType::Attacker);
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(4, 5, 3, 5), true);
        assert_eq!(board.tile_matrix[3][4], None);
    }

    #[test]
    fn cannot_take_defender_with_occupied_throne_test() {
        let mut board = Board::new();
        board.tile_matrix[3][3] = Some(PieceType::King);
        board.tile_matrix[3][4] = Some(PieceType::Defender);
        board.tile_matrix[4][5] = Some(PieceType::Attacker);
        board.current_player = Players::Attackers;
        assert_eq!(board.make_move(4, 5, 3, 5), true);
        assert_eq!(board.tile_matrix[3][4], Some(PieceType::Defender));
    }

    #[test]
    fn attackers_win_when_king_captured_test() {
        let mut board = Board::new();
        board.tile_matrix[1][1] = Some(PieceType::Defender);
        board.tile_matrix[1][2] = Some(PieceType::Attacker);
        assert_eq!(board.winner, None);
        board.evaluate_victory_conditions();
        assert_eq!(board.winner, Some(Players::Attackers));
    }

    #[test]
    fn defenders_win_when_king_reaches_corner_test() {
        let mut board = Board::new();
        board.tile_matrix[1][1] = Some(PieceType::Defender);
        board.tile_matrix[1][2] = Some(PieceType::Attacker);
        board.tile_matrix[0][0] = Some(PieceType::King);
        assert_eq!(board.winner, None);
        board.evaluate_victory_conditions();
        assert_eq!(board.winner, Some(Players::Defenders));
    }

    #[test]
    fn defender_can_pass_but_not_land_empty_throne_test() {
        let mut board = Board::new();
        board.current_player = Players::Defenders;
        board.tile_matrix[3][4] = Some(PieceType::Defender);
        assert_eq!(board.can_move(3, 4, 3, 3), false);
        assert_eq!(board.can_move(3, 4, 3, 2), true);
    }

    #[test]
    fn attacker_can_pass_but_not_land_empty_throne_test() {
        let mut board = Board::new();
        board.current_player = Players::Attackers;
        board.tile_matrix[3][4] = Some(PieceType::Attacker);
        assert_eq!(board.can_move(3, 4, 3, 3), false);
        assert_eq!(board.can_move(3, 4, 3, 2), true);
    }

    #[test]
    fn defenders_with_no_available_moves_loses_test() {
        let mut board = Board::new();
        board.current_player = Players::Defenders;
        board.tile_matrix[0][3] = Some(PieceType::King);
        board.tile_matrix[0][4] = Some(PieceType::Attacker);
        board.tile_matrix[0][2] = Some(PieceType::Attacker);
        board.tile_matrix[1][3] = Some(PieceType::Attacker);
        let moves = board.get_available_moves();
        assert_eq!(moves.len(), 0);
        board.evaluate_victory_conditions();
        assert_eq!(board.winner, Some(Players::Attackers));
    }
    
    #[test]
    fn is_on_or_next_to_throne_test() {
        assert_eq!(Board::is_on_or_next_to_throne(3, 3), true);
        assert_eq!(Board::is_on_or_next_to_throne(4, 3), true);
        assert_eq!(Board::is_on_or_next_to_throne(2, 3), true);
        assert_eq!(Board::is_on_or_next_to_throne(3, 4), true);
        assert_eq!(Board::is_on_or_next_to_throne(3, 2), true);
        assert_eq!(Board::is_on_or_next_to_throne(4, 4), false);
        assert_eq!(Board::is_on_or_next_to_throne(2, 2), false);
        assert_eq!(Board::is_on_or_next_to_throne(4, 2), false);
        assert_eq!(Board::is_on_or_next_to_throne(2, 4), false);
        assert_eq!(Board::is_on_or_next_to_throne(1, 1), false);
        assert_eq!(Board::is_on_or_next_to_throne(1, 2), false);
        assert_eq!(Board::is_on_or_next_to_throne(0, 0), false);
    }
}
